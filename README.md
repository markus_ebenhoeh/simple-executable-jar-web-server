# simple-executable-jar-web-server

A very simple test web server to test the the OpenShift Spring-Boot-Jar-Runner Cartridge

This single class web server is packaged as an executable JAR and returns some information about the execution environment (system environment, system properties, request headers, time zone).

The purpose of this is to be packaged as part of the OpenShift Spring-Boot-Jar-Runner Cartridge (https://bitbucket.org/markus_ebenhoeh/openshift-cartridge-spring-boot-jar-runner.git). Therefore the JAR-file has to be as small as possible and therefore there are no dependencies on any other libraries.

The jar depends on the Java SDK rt.jar though.
 

See notes about running this in Cloud Foundry here : http://mnm.at/markus/2014/06/15/cloud-foundry-environment-notes/



