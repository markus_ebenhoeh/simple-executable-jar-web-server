package at.mnm.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

@SuppressWarnings("restriction")
public class SimpleTestWebServer implements HttpHandler {

   private static final String YYYY_MM_DD_HH_MM_Z = "yyyy-MM-dd HH:mm Z";

   public static void main(String[] args) throws Exception {

      String serverAddress = System.getProperty("server.address", "localhost");
      int serverPort = Integer.getInteger("server.port", 8080);

      System.out.println("going to listen on " + serverAddress + ":" + serverPort);

      SimpleTestWebServer simpleTestWebServer = new SimpleTestWebServer();
      simpleTestWebServer.printEnvironmentInformationToSystemOut(args);

      HttpServer server = HttpServer.create(new InetSocketAddress(serverAddress, serverPort), 0);
      server.createContext("/", simpleTestWebServer);
      server.setExecutor(null);
      server.start();
      System.out.println("started server on " + serverAddress + ":" + serverPort);
   }

   private void printEnvironmentInformationToSystemOut(String[] args) {
      System.out.println("Command Line Arguments:");
      for (String argument : args) {
         System.out.println("   " + argument);
      }

      PrintWriter pw = new PrintWriter(System.out);

      System.out.println("System Properties:");
      printMap(pw, System.getProperties());
      pw.flush();

      System.out.println("Environment Proeprties:");
      printMap(pw, System.getenv());
      pw.flush();

      System.out.println("--- end printEnvironmentInformationToSystemOut");

   }

   public void handle(HttpExchange t) throws IOException {
      String reponseBody = generateResponseBody(t);
      addNoCacheHeaders(t.getResponseHeaders());

      t.sendResponseHeaders(200, reponseBody.length());
      OutputStream os = t.getResponseBody();
      os.write(reponseBody.getBytes());
      os.close();
   }

   private String generateResponseBody(HttpExchange t) {
      StringWriter stringWriter = new StringWriter();
      PrintWriter pw = new PrintWriter(stringWriter);

      pw.println("<html><head><title>Simple Test Web Server Application</title>");
      pw.println("<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css\">");
      pw.println("<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css\">");
      pw.println("</head>");
      pw.println("<body>");
      pw.println("<div class=\"container\">");

      pw.println("<h1>Simple Test Web Server Application</h1>");
      pw.println("<h2>Hi, there!</h2><div>I am a simple server that can return only this response, ever.</div>");
      pw.println("<div>I am supposed to help you check if everything is fine and dandy. First off: you're reading this, so mission accomplished!</div>");

      pw.println("<h2>Here is what I know about the server:</h2>");

      pw.printf("<div>The server thinks it is '%s'</div>\n", new SimpleDateFormat(YYYY_MM_DD_HH_MM_Z).format(new Date()));
      pw.printf("<div>UTC time is '%s'</div>\n", getUtcTimeStamp());

      pw.println("The server adress : " + t.getLocalAddress());

      pw.println("<div><h2>Here is what I know about the server's environment:</h2><pre>");
      printMap(pw, System.getenv());
      pw.println("</pre></div>");

      pw.println("<div><h2>Here is what I know about the server's system properties:</h2><pre>");
      printMap(pw, System.getProperties());
      pw.println("</pre></div>");

      pw.println("<div><h2>Here is what I know about you :</h2>");

      pw.println("<h3>Your adress:</h3> <pre>" + t.getRemoteAddress() + "</pre>");

      pw.println("<h3>Your request headers:</h3><pre>");
      printMap(pw, t.getRequestHeaders());
      pw.println("</pre>");
      pw.println("</div>");

      pw.println("</div>");
      pw.println("</body>");

      pw.flush();
      String reponseBody = stringWriter.toString();
      return reponseBody;
   }

   @SuppressWarnings({ "rawtypes", "unchecked" })
   private void printMap(PrintWriter pw, Map map) {
      TreeMap tm = new TreeMap(map);
      for (Object key : tm.keySet()) {
         pw.println("   '" + key + "' = '" + map.get(key) + "'");
      }
   }

   private void addNoCacheHeaders(Headers responseHeaders) {
      responseHeaders.add("Cache-Control", "no-cache");
      responseHeaders.add("Pragma", "no-cache");
      responseHeaders.add("Expires", "Fri, 01 Jan 1990 00:00:00 GMT");
   }

   private String getUtcTimeStamp() {
      DateFormat df = new SimpleDateFormat(YYYY_MM_DD_HH_MM_Z);
      df.setTimeZone(TimeZone.getTimeZone("UTC"));
      return df.format(new Date());
   }

}
